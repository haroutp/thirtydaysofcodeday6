﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Day6
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = Convert.ToInt32(Console.ReadLine());
            var listOfStrings = new List<string>();

            while(i > 0){
                string s = Console.ReadLine();
                listOfStrings.Add(s);
                i--;
            }
            
            List<string> newStrings = new List<string>();
            
            foreach (var item in listOfStrings)
            {
                List<string> even = new List<string>();
                List<string> odd = new List<string>();

                for (int j = 0; j < item.Length; j++)
                {
                    if(j % 2 == 0){
                        even.Add(Convert.ToString(item[j]));
                    }else{
                        odd.Add(Convert.ToString(item[j]));
                    }
                }
                newStrings.Add(string.Join("", even) + " " + string.Join("", odd));
            }

            foreach (var item in newStrings)
            {
                System.Console.WriteLine(item);
            }
        }
    }
}
